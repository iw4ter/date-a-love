<?php
require_once '../settings.php';

$user = new User($pdo);

if (!empty($_POST)) {
    $errors = errors_check($_POST);
    if (empty($errors)) {
        $user->add($_POST);
        redirect('register.php');
    }

} ?>
<html>
    <head>
        <title>Registration</title>
        <link rel="stylesheet" type="text/css" href="../css/login_register.css">
    </head>

    <? if (!empty($errors)): ?>
        <div class="errors">
            <? errors_print($errors) ?>
        </div>
    <? endif ?>

    <div class="wrap">
        <form class="register_form" method="POST" action="/users/register.php">
            <div class="block_name">
                <input class="form_out" id="input_name" type="text" name="name"/>
                <label for="input_name"> Введите имя</label>
            </div>
            <div class="block_email">
                <input class="form_out" id="input_email" type="text" name="email"/>
                <label for="input_email"> Введите имейл</label>
            </div>
            <div class="block_pass">
                <input class="form_out" id="input_pass" type="text" name="password"/>
                <label for="input_pass"> Введите пароль</label>
            </div>
            <div class="submit_button">
                <input class="form_in" name="action" type="submit"/>
            </div>
        </form>
    </div>

</html>
<?




function errors_check($POST)
{
    $errors = array();

    $errors[] = empty($POST['name']) ?  'отсутствует Имя!' : false;
    $errors[] = empty($POST['email']) ?  'отсутствует имейл!' : false;
    $errors[] = empty($POST['password']) ?  'отсутствует пароль!' : false;

    return $errors;
}

function errors_print($errors)
{
    foreach ($errors as $error => $value) {
        echo $value . "</br>";
    }
}