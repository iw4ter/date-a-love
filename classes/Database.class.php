<?php
class Db {

    function __construct($pdo){
        $this->pdo = $pdo;
    }

    //TODO пусть возвращает ID созданной записи
    public function add($database, $values){

        $keys = $this->fetchKeys($values);
        $values = $this->fetchValues($values);

        $this->pdo->query("INSERT INTO {$database} ({$keys}) VALUES({$values})");

        //return только что созданный id (ПДО функция)
    }

    //TODO функця edit(). Возвращает true/false
    public function edit(){

    }

    public function fetchValues($data){
        return "'" . implode("', '", array_values($data)) . "'";
    }

    public function fetchKeys($data){
        return implode(", ", array_keys($data));
    }

}