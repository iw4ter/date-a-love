<?php
require_once 'settings.php';

if(empty($_SESSION['user']['id']))
    redirect('/users/login.php');
