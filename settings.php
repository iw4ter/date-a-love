<?

//настройки
$connection['username'] = 'root';
$connection['password']= '';
$connection['database'] = 'datealove';
$connection['host'] = 'localhost';

//подключим классы
require_once 'utils.php';
require_once 'classes/Database.class.php';
require_once 'classes/User.class.php';
require_once 'classes/Date.class.php';

//инициируем классы
//($host, $db, $user, $pass)

$dsn = "mysql:host={$connection['host']};dbname={$connection['database']};";
$opt = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);
$pdo = new PDO($dsn, $connection['username'], $connection['password'], $opt);